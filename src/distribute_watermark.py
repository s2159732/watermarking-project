import numpy as np

def distribute_watermark(watermark):
    '''
    Gets 4nx4n array containing a watermark and returns an array containing sixteen lists of the distributed watermark.
    :param watermark: a 4nx4n array containing a watermark
    :return: an array containing sixteen lists of the distributed watermark
    '''

    dim_size = watermark.shape[0]

    dist_wm_pointer = 0
    dist_wm = np.zeros((16, watermark.size // 16))

    for ii in range(0, dim_size, 4):
        for jj in range(0, dim_size, 4):
            block = watermark[ii:ii+4, jj:jj+4].flatten()
            for kk in range(16):
                dist_wm[kk, dist_wm_pointer] = block[((kk*9) % 16)]
            dist_wm_pointer += 1
    return dist_wm

def remake_distributed_watermark(dist_wm):
    dim_size = int(np.sqrt(dist_wm.size))
    watermark = np.zeros((dim_size, dim_size))
    for kk in range(16):
        start_row = ((9 * kk) % 16) // 4
        start_column = ((9 * kk) % 16) % 4
        wm_pointer = 0
        for ii in range(start_row, dim_size, 4):
            for jj in range(start_column, dim_size, 4):
                watermark[ii, jj] = dist_wm[kk, wm_pointer]
                wm_pointer += 1
    return watermark

def frame_index_to_bin_array(index, size=4):
    bin_array = np.zeros(size)
    index_string = bin(index)[2:]
    for ii in range(len(index_string)):
        bin_array[-1 - ii] = int(index_string[-1 - ii])
    return bin_array

def bin_array_to_int(bin_array):
    res = 0
    for ii in range(len(bin_array)):
        bit = bin_array[-1 - ii]
        if bit == 1:
            res += 2**ii
    return res