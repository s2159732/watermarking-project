from prediction_modes import *

def select_blocks(method_map):
    '''
    Makes a list containing all the blocks eligible for data hiding
    The coordinates are in block coordinates so multiply them by four for the coordinate of the top left pixel

    :param method_map:
    :return:
    '''
    height = method_map.shape[0]
    width = method_map.shape[1]
    eligible = list()

    for ii in range(height - 1):
        for jj in range(1, width - 1):
            c1 = condition_1(method_map, ii, jj)
            c2 = condition_2(method_map, ii, jj)
            c3 = condition_3(method_map, ii, jj)
            c4 = condition_4(method_map, ii, jj)
            if c1 and c2 and c3 and c4:
                eligible.append((ii, jj))

    return eligible

def condition_1(method_map, i, j):
    R = method_map[i, j+1]
    return all([m in [0, 3, 7, 10] for m in R])

def condition_2(method_map, i, j):
    UL = method_map[i+1, j-1]
    U = method_map[i+1, j]
    return all([m in [0, 1, 2, 4, 5, 6, 8, 10, 11, 12, 1] for m in UL] + [m in [1, 8, 11] for m in U])

def condition_3(method_map, i, j):
    UR = method_map[i+1, j+1]
    return all([m in [0, 1, 2, 3, 7, 8, 10, 11, 12, 13] for m in UR])

def condition_4(method_map, i, j):
    return len(method_map[i, j]) == 0

def check_single_block_mmap(i, j, method_map):
    c1 = condition_1(method_map, i, j)
    c2 = condition_2(method_map, i, j)
    c3 = condition_3(method_map, i, j)
    c4 = condition_4(method_map, i, j)
    return c1 and c2 and c3 and c4

def single_region_mmap(i, j, y_img):
    method_map = build_method_map(2, 3)
    i_left_corner = i * 4
    j_left_corner = (j - 1) * 4
    for ii in range(0, 2):
        for jj in range(0, 3):
            for method in [0, 1, 2, 3, 4, 5, 6, 7, 8]:
                i_current_block = i_left_corner + (ii * 4)
                j_current_block = j_left_corner + (jj * 4)
                block, dict = get_4block(i_current_block, j_current_block, y_img)
                if match_prediction_4block(block, dict, method):
                    method_map[ii, jj].append(method)
    return method_map

def check_single_block(i, j, y_img):
    method_map = single_region_mmap(i, j, y_img)
    c1 = condition_1(method_map, 0, 1)
    c2 = condition_2(method_map, 0, 1)
    c3 = condition_3(method_map, 0, 1)
    c4 = condition_4(method_map, 0, 1)
    return c1 and c2 and c3

def check_quadrant(blocks_used):
    quadrant_size = 32

    if len(blocks_used) == 0:
        return [(quadrant_size//2, quadrant_size//2)]

    quadrant = np.ones((quadrant_size, quadrant_size)) * -1

    for block in blocks_used:
        quadrant[block] = 0

    blocks_had = len(blocks_used)
    current = blocks_used
    tentative = list()
    it = 1
    while blocks_had < quadrant_size ** 2:
        tentative = list()
        for block in current:
            i = block[0]
            j = block[1]
            if i - 1 >= 0:
                if quadrant[i - 1, j] == -1:
                    quadrant[i - 1, j] = it
                    tentative.append((i - 1, j))
                if j - 1 >= 0:
                    if quadrant[i - 1, j - 1] == -1:
                        quadrant[i - 1, j - 1] = it
                        tentative.append((i - 1, j - 1))
                if j + 1 < quadrant_size:
                    if quadrant[i - 1, j + 1] == -1:
                        quadrant[i - 1, j + 1] = it
                        tentative.append((i - 1, j + 1))
            if i + 1 < quadrant_size:
                if quadrant[i + 1, j] == -1:
                    quadrant[i + 1, j] = it
                    tentative.append((i + 1, j))
                if j - 1 >= 0:
                    if quadrant[i + 1, j - 1] == -1:
                        quadrant[i + 1, j - 1] = it
                        tentative.append((i + 1, j - 1))
                if j + 1 < quadrant_size:
                    if quadrant[i + 1, j + 1] == -1:
                        quadrant[i + 1, j + 1] = it
                        tentative.append((i + 1, j + 1))
            if j - 1 >= 0:
                if quadrant[i, j - 1] == -1:
                    quadrant[i, j - 1] = it
                    tentative.append((i, j - 1))
            if j + 1 < quadrant_size:
                if quadrant[i, j + 1] == -1:
                    quadrant[i, j + 1] = it
                    tentative.append((i, j + 1))

        blocks_had += len(tentative)
        current = tentative
        it += 1

    return tentative