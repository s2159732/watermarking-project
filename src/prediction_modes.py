import cv2
import numpy as np

def get_y_img(img):
    yuv_img = cv2.cvtColor(img, cv2.COLOR_RGB2YUV)
    y_img = yuv_img[:, :, 0]
    return y_img

def get_16block(i, j, img, get_upper=True, get_left=True):
    pred_block = img[i:i+16, j:j+16]

    bound_dict = dict()
    if get_upper:
        bound_dict["H"] = img[i-1, j:j+16]

    if get_left:
        bound_dict["V"] = img[i:i+16, j-1]

    if get_upper and get_left:
        bound_dict["M"] = [img[i-1, j-1]]

    return pred_block, bound_dict

def match_prediction_16block(pred_block, bound_dict, method):
    if method == 0:
        would_be_pred_block = np.zeros((16,16))
        for ii in range(16):
            for jj in range(16):
                would_be_pred_block[ii,jj] = bound_dict["H"][jj]

    elif method == 1:
        would_be_pred_block = np.zeros((16,16))
        for ii in range(16):
            for jj in range(16):
                would_be_pred_block[ii,jj] = bound_dict["V"][ii]

    elif method == 2:
        mean = np.mean(bound_dict["V"]) + np.mean(bound_dict["H"])
        mean /= 2
        mean += 0.5
        would_be_pred_block = np.ones((16, 16)) * int(mean)

    elif method == 3:
        p = np.concatenate((bound_dict["H"], bound_dict["M"]))
        H = np.sum([(x+1) * (int(p[8+x]) - int(p[6-x])) for x in range(8)])
        p = np.concatenate((bound_dict["V"], bound_dict["M"]))
        V = np.sum([(y+1) * (int(p[8+y]) - int(p[6-y])) for y in range(8)])

        a = 16 * (int(bound_dict["V"][15]) + int(bound_dict["H"][15]))
        b = (5 * H + 32) >> 6
        c = (5 * V + 32) >> 6

        would_be_pred_block = np.zeros((16, 16))
        for ii in range(16):
            for jj in range(16):
                would_be_pred_block[ii, jj] = np.clip((a + (b * (jj - 7)) + (c * (ii - 7)) + 16) >> 5, 0, 255)

    return all((pred_block == would_be_pred_block).flatten())


def get_4block(i, j, img, get_upper=True, get_left=True, get_upper_right=True):
    pred_block = img[i:i+4, j:j+4]

    bound_dict = dict()
    if get_upper and not get_upper_right:
        upper_bound_pixels = img[i-1, j:j+4]
        letters = "ABCD"
        for k in range(4):
            bound_dict[letters[k]] = upper_bound_pixels[k]

    if get_left:
        left_bound_pixels = img[i:i+4, j-1]
        letters = "IJKL"
        for k in range(4):
            bound_dict[letters[k]] = left_bound_pixels[k]

    if get_left and (get_upper or get_upper_right):
        bound_dict["M"] = img[i-1, j-1]

    if get_upper_right:
        upper_bound_pixels = img[i - 1, j:j+8]
        letters = "ABCDEFGH"
        for k in range(8):
            bound_dict[letters[k]] = upper_bound_pixels[k]

    return pred_block, bound_dict

def match_prediction_4block(pred_block, bound_dict, method):
    if method == 0 or method == 1:
        if method == 0:
            method_block = np.array([
                ['A', 'B', 'C', 'D'],
                ['A', 'B', 'C', 'D'],
                ['A', 'B', 'C', 'D'],
                ['A', 'B', 'C', 'D']
            ])
        elif method == 1:
            method_block = np.array([
                ['A', 'A', 'A', 'A'],
                ['B', 'B', 'B', 'B'],
                ['C', 'C', 'C', 'C'],
                ['D', 'D', 'D', 'D']
            ])

        for ii in range(4):
            for jj in range(4):
                if pred_block[ii, jj] != bound_dict[method_block[ii, jj]]:
                    return False
        return True

    elif method == 2:
        mean = 0
        for letter in "ABCDIJKL":
            mean += bound_dict[letter]
        mean /= 8
        mean += 0.5
        return all((pred_block.flatten() == int(mean)))

    else:
        if method == 3:
            method_block = np.array([
                ['ABBC', 'BCCD', 'CDDE', 'DEEF'],
                ['BCCD', 'CDDE', 'DEEF', 'EFFG'],
                ['CDDE', 'DEEF', 'EFFG', 'FGGH'],
                ['DEEF', 'EFFG', 'FGGH', 'GHHH']
            ])
        elif method == 4:
            method_block = np.array([
                ['IMMA', 'MAAB', 'ABBC', 'BCCD'],
                ['JIIM', 'IMMA', 'MAAB', 'ABBC'],
                ['KJJI', 'JIIM', 'IMMA', 'MAAB'],
                ['LKKJ', 'KJJI', 'JIIM', 'IMMA']
            ])
        elif method == 5:
            method_block = np.array([
                ['MA', 'AB', 'BC', 'CD'],
                ['IMMA', 'MAAB', 'ABBC', 'BCCD'],
                ['MIIJ', 'MA', 'AB', 'BC'],
                ['IJJK', 'IMMA', 'MAAB', 'ABBC']
            ])
        elif method == 6:
            method_block = np.array([
                ['MI', 'IMMA', 'MAAB', 'ABBC'],
                ['IJ', 'MIIJ', 'MI', 'IMMA'],
                ['JK', 'IJJK', 'IJ', 'MIIJ'],
                ['KL', 'JKKL', 'JK', 'IJJK']
            ])
        elif method == 7:
            method_block = np.array([
                ['AB', 'BC', 'CD', 'DE'],
                ['ABBC', 'BCCD', 'CDDE', 'DEEF'],
                ['BC', 'CD', 'DE', 'EF'],
                ['BCCD', 'CDDE', 'DEEF', 'EFFG']
            ])
        elif method == 8:
            method_block = np.array([
                ['IJ', 'IJJK', 'JK', 'JKKL'],
                ['JK', 'JKKL', 'KL', 'KLLL'],
                ['KL', 'KLLL', 'L', 'L'],
                ['L', 'L', 'L', 'L']
            ])

        for ii in range(4):
            for jj in range(4):
                value = 0
                for kk in method_block[ii, jj]:
                    value += bound_dict[kk]
                value /= len(method_block[ii, jj])
                value += 0.5
                if pred_block[ii, jj] != int(value):
                    return False
        return True

def go_through_img_4block(img, method, ignore_check = False, ignore_map = None, not_ignore_sign = 9):
    height, width = img.shape[0], img.shape[1]
    blocks_height = height // 4
    blocks_width = width // 4

    result = list()

    for ii in range(1, blocks_height):
        for jj in range(1, blocks_width - 1):
            i = ii * 4
            j = jj * 4
            if not ignore_check or ignore_map[ii, jj] == not_ignore_sign:
                block, dict = get_4block(i, j, img)
                if match_prediction_4block(block, dict, method):
                    result.append((ii, jj))
    return result

def go_through_img_16block(img, method, ignore_check = False, ignore_map = None, not_ignore_sign = 9):
    height, width = img.shape[0], img.shape[1]
    blocks_height = height // 16
    blocks_width = width // 16

    result = list()

    for ii in range(1, blocks_height):
        for jj in range(1, blocks_width - 1):
            i = ii * 16
            j = jj * 16
            if not ignore_check or ignore_map[ii, jj] == not_ignore_sign:
                block, dict = get_16block(i, j, img)
                if match_prediction_16block(block, dict, method):
                    result.append((ii, jj))
    return result

def define_method_map(img, verbose=False):
    height, width = img.shape[0], img.shape[1]
    blocks_height = height // 4
    blocks_width = width // 4

    method_map = build_method_map(blocks_height, blocks_width)

    if verbose:
        print("16x16")
    for method in [0, 1, 2, 3]:
        if verbose:
            print(f"{method=}")
        res = go_through_img_16block(img, method)
        for block in res:
            method_map[block[0], block[1]].append(method + 10)
            method_map[block[0]+1, block[1]].append(method + 10)
            method_map[block[0], block[1]+1].append(method + 10)
            method_map[block[0]+1, block[1]+1].append(method + 10)
        if verbose:
            print(f"method {method} had {len(res)} corresponding prediction blocks")

    if verbose:
        print("4x4")
    for method in [0, 1, 2, 3, 4, 5, 6, 7, 8]:
        if verbose:
            print(f"{method=}")
        res = go_through_img_4block(img, method)
        for block in res:
            method_map[block].append(method)
        if verbose:
            print(f"method {method} had {len(res)} corresponding prediction blocks")

    if verbose:
        print(f"fraction of non-predicted blocks {1 - ((np.sum(len(method_map) > 0)) / (blocks_height*blocks_width))}")
    return method_map

def build_method_map(height, width):
    mmap = np.ones((height, width), dtype=list)
    for ii in range(height):
        for jj in range(width):
            mmap[ii, jj] = list()
    return mmap

def save_method_map(file_path, mmap):
    f = open(file_path, "w")
    f.write(str(mmap.shape) + ";")
    flatmap = mmap.flatten()
    for row in flatmap:
        string = str(row)
        for char in "[] ":
            string = string.replace(char, '')
        f.write(string + ";")
    f.close()

def read_method_map_file(file_path):
    f = open(file_path, "r")
    f_string = f.read()

    to_delete = "()[] "
    for char in to_delete:
        f_string = f_string.replace(char, '')
    f_list = f_string.split(";")

    shape = f_list[0].split(",")
    height = int(shape[0])
    width = int(shape[1])
    mmap = build_method_map(height, width)
    f_list = f_list[1:-1]

    for ii in range(len(f_list)):
        if len(f_list[ii]) > 0:
            methods = f_list[ii].split(",")
            i = ii // width
            j = ii % width
            for method in methods:
                mmap[i, j].append(int(method))

    return mmap