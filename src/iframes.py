import av
import cv2


def get_iframes(video_path, folder_path=False, stop_at_16=False):
    container = av.open(video_path)
    iframes = list()
    for frame in container.decode(video=0):
        if frame.key_frame:
            iframes.append(frame.index)
            if folder_path:
                frame.to_image().save(f"{folder_path}/frame_{frame.index}.jpg")
        if len(iframes) == 16 and stop_at_16:
            break
    return iframes
