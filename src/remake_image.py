import numpy as np
import cv2

def insert_y(y_img, img_path):
    img = cv2.imread(img_path)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2YUV)
    height = img.shape[0]
    width = img.shape[1]
    new_img = np.zeros((height, width, 3))
    for i in range(height):
        for j in range(width):
            new_img[i, j, 0] = y_img[i, j]
            new_img[i, j, 1:3] = img[i, j, 1:3]
    new_img = new_img.astype('uint8')
    rgb_img = cv2.cvtColor(new_img, cv2.COLOR_YUV2RGB)
    return rgb_img