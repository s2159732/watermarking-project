import cv2

from iframes import *
from block_selection import *
from prediction_modes import *
from ict_transform import *
from distribute_watermark import *
from embedding import *
from remake_image import *
from watermark_to_image import *
from error_correcting_code import *

def generate_random_code(n):
    code = np.zeros(n)
    for ii in range(n):
        if np.random.random() > 0.5:
            code[ii] = 0
        else:
            code[ii] = 1

    return code

def compressed_mushroom():
    wm = image_to_watermark("./img/watermark.jpg").flatten()
    yimg = embed_watermark_quadrant_two("./img/mushroom_compressed.jpg", wm)
    img = insert_y(yimg, "./img/mushroom_compressed.jpg")
    print("done")
    cv2.imwrite("test.jpg", img)
    img = cv2.imread("test.jpg")
    yimg = get_y_img(img)
    print(yimg.shape)
    ewm = extract_watermark_quadrant_two(yimg, len(wm))
    print((wm == ewm).all())
    ewm = np.array(ewm).reshape(80, 80)
    watermark_to_image(ewm)

def main():
    img = cv2.imread("frames/frame_0.jpg")
    yimg = get_y_img(img)
    print(yimg.shape)
    ewm = extract_watermark_quadrant_two(yimg, 808)
    print(ewm)
