import cv2
import numpy as np


def watermark_to_image(watermark, show = True):
    img = np.zeros((watermark.shape[0], watermark.shape[1], 3))
    for ii in range(watermark.shape[0]):
        for jj in range(watermark.shape[1]):
            if watermark[ii, jj] == 0:
                img[ii, jj] = [255, 255, 255]
    if show:
        cv2.imshow("watermark", img)
        cv2.waitKey((0))

    return img

def image_to_watermark(img_path):
    img = cv2.imread(img_path)
    bit_img = np.zeros(img.shape[0:2], dtype = int)
    for ii in range(img.shape[0]):
        for jj in range(img.shape[1]):
            if np.sum(img[ii, jj]) < 700:
                bit_img[ii, jj] = 1
    return bit_img

def rescale_watermark(old_watermark, height, width):
    watermark = np.zeros((height, width))
    sfh = old_watermark.shape[0] / height
    sfw = old_watermark.shape[1] / width

    for ii in range(height):
        for jj in range(width):
            reg = old_watermark[int(ii*sfh):int((ii*sfh)+sfh), int(jj*sfw):int((jj*sfw) + sfw)]
            bit = round(np.mean(reg.flatten()))
            watermark[ii,jj] = bit

    return watermark