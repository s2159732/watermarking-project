import numpy as np

def dummy_encode(msg):
    return np.concatenate((msg, msg))

def dummy_decode(msg):
    return msg[:len(msg)//2]

def triplication_encode(msg):
    return np.concatenate((msg, msg, msg))

def triplication_decode(msg):
    decoded_msg = np.zeros(len(msg) // 3)
    length = len(decoded_msg)

    for ii in range(length):
        if msg[ii] + msg[length + ii] + msg[2 * length + ii] >= 2:
            decoded_msg[ii] = 1

    return decoded_msg
