from iframes import *
from block_selection import *
from prediction_modes import *
from ict_transform import *
from distribute_watermark import *
from embedding import *
from remake_image import *
from watermark_to_image import *
from error_correcting_code import *
import numpy as np

def steg_extract_video(video_path, save_watermark = False):
    iframes = get_iframes(video_path)
    frame_serials = np.zeros(16)
    watermark = np.zeros((16, 400))

    for ii in range(len(iframes)):
        print(iframes[ii])
        frame = cv2.imread(f"frames/frame_{iframes[ii]}.jpg")
        yimg = get_y_img(frame)
        succes, msg = extract_watermark_quadrant_two(yimg, 808)
        msg = dummy_decode(msg)
        serial = bin_array_to_int(msg[:4])
        msg = msg[4:]
        if succes:
            if frame_serials[serial] == 0 or frame_serials[serial] == 2:
                frame_serials[serial] = 1
                watermark[serial] = msg
                print(f"{np.sum(frame_serials == 1)}/16")
                if (frame_serials == 1).all():
                    break
        else:
            if frame_serials[serial] == 0:
                frame_serials[serial] = 2
                watermark[serial] = msg

    watermark = remake_distributed_watermark(watermark)
    watermark_to_image(watermark)
    return watermark

steg_extract_video("videos/desk2.mp4")