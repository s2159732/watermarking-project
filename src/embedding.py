import numpy as np
from block_selection import *
from ict_transform import *

lower_bound = 3
upper_bound = 5

def embeddable_bits(matrix, n=0):
    embeddable = 0
    for i in range(4):
        for j in range(4):
            if matrix[i, j] == n and upper_bound >= i + j >= lower_bound:
                embeddable += 1
    return embeddable

def embedded_bits(matrix, n=0):
    embedded = 0
    for i in range(4):
        for j in range(4):
            if (matrix[i, j] == n or matrix[i, j] == n + 1) and upper_bound >= i + j >= lower_bound:
                embedded += 1
    return embedded

def find_embedded_bit_block(y_img, mmap, loc, n=0):
    locs_had = 0
    elig_blocks = select_blocks(mmap)
    block_loc = 0

    while loc >= locs_had:
        block_i = elig_blocks[block_loc][0] * 4
        block_j = elig_blocks[block_loc][1] * 4
        block = y_img[block_i:block_i + 4, block_j:block_j + 4]
        dct_block = dct(block)
        prev_locs_had = locs_had
        if embedded_bits(dct_block) != 15:
            print(embedded_bits(dct_block), block_loc)
        locs_had += embedded_bits(dct_block)
        block_loc += 1

    block_loc -= 1
    index_in_block = loc - prev_locs_had

    return block_loc, index_in_block


def embed(matrix, code, n=0, reversible = True):
    result_matrix = np.zeros((4, 4))
    bit_pointer = 0

    for i in range(4):
        for j in range(4):
            if upper_bound >= i + j >= lower_bound:
                yij = matrix[i, j]

                if abs(yij) != n and reversible:
                    if yij >= 0 and abs(yij) > n:
                        result_matrix[i, j] = yij + 1
                    elif yij <= 0 and abs(yij) > n:
                        result_matrix[i, j] = yij - 1
                    elif abs(yij) < n:
                        result_matrix[i, j] = yij

                elif abs(yij) != n and not reversible:
                    if yij == n + 1:
                        result_matrix[i, j] = n + 2
                    elif yij == n - 1:
                        result_matrix[i, j] = n - 2
                    else:
                        result_matrix[i, j] = yij

                elif abs(yij) == n:
                    bit = code[bit_pointer]
                    if bit_pointer < len(code) - 1:
                        bit_pointer += 1
                    if bit == 1:
                        if yij >= 0:
                            result_matrix[i, j] = yij + 1
                        elif yij < 0:
                            result_matrix[i, j] = yij - 1
                    elif bit == 0:
                        result_matrix[i, j] = yij
            else:
                result_matrix[i, j] = matrix[i, j]

    return result_matrix

def extraction(matrix, n=0):
    code = list()

    for i in range(4):
        for j in range(4):
            if upper_bound >= i + j >= lower_bound:
                yij = matrix[i, j]
                if yij == n:
                    code.append(0)
                elif yij == n + 1:
                    code.append(1)
    return code

def embed_watermark_linear_in_image(img_path, watermark):
    img = cv2.imread(img_path)
    y_img = get_y_img(img)

    wm_pointer = 0
    height_in_blocks = y_img.shape[0] // 4
    width_in_blocks = y_img.shape[1] // 4
    used_blocks = list()
    wm_history = list()
    dep_blocks = list()

    for ii in range(1, height_in_blocks, 1):
        for jj in range(1, width_in_blocks - 2, 1):
            block_skipped = False
            if check_single_block(ii, jj, y_img):
                used_blocks.append((ii, jj))
                # Find pixel coordinates of the block
                block_i = ii * 4
                block_j = jj * 4
                # Get the block
                block = y_img[block_i:block_i + 4, block_j:block_j + 4]
                # transform the block
                block_dct = dct(block)
                # Embed the bits
                n_bits = min(embeddable_bits(block_dct) - 1, len(watermark) - wm_pointer)
                bits_in_block = [1] + watermark[wm_pointer: wm_pointer + n_bits]
                e_block = embed(block_dct, bits_in_block, reversible = True)
                # Retransform the block
                block_embedded = idct(e_block)
                # test if the block is viable
                if (block_embedded < 0).any():
                    # If doesn't work add a code beginning with zero
                    bits_in_block = [0] * (n_bits + 1)
                    e_block = embed(block_dct, bits_in_block)
                    block_embedded = idct(e_block)
                    block_skipped = True
                    dep_blocks.append((ii, jj))
                # Put the block back
                y_img[block_i:block_i + 4, block_j:block_j + 4] = block_embedded
                #Check whether the mutation created a prediction
                if not check_single_block(ii, jj, y_img):
                    #If so add a code with a leading 0
                    bits_in_block = [0] * (n_bits + 1)
                    e_block = embed(block_dct, bits_in_block)
                    block_embedded = idct(e_block)
                    #Put the new block in
                    y_img[block_i:block_i + 4, block_j:block_j + 4] = block_embedded
                    block_skipped = True

                if not block_skipped:
                    # Otherwise update wm_pointer
                    wm_pointer += n_bits
                    wm_history.append((wm_pointer, (ii, jj)))

            if wm_pointer >= len(watermark):
                return y_img, used_blocks

def extract_watermark_linear_in_y_image(y_img, watermark_length):
    used_blocks = list()
    watermark = list()
    height_in_blocks = y_img.shape[0] // 4
    width_in_blocks = y_img.shape[1] // 4
    wm_history = list()

    for ii in range(0, height_in_blocks, 4):
        for jj in range(1, width_in_blocks, 4):
            if check_single_block(ii, jj, y_img):
                used_blocks.append((ii,jj))
                # Find pixel coordinates of the block
                block_i = ii * 4
                block_j = jj * 4
                # Get the block
                block = y_img[block_i:block_i + 4, block_j:block_j + 4]
                # transform the block
                block_dct = dct(block)
                # extract the bits
                extracted = extraction(block_dct)
                # Check whether extracted is valid
                if extracted[0] == 1:
                    watermark += extracted[1:]
                    wm_history.append((len(watermark), (ii, jj)))
            if len(watermark) >= watermark_length:
                return watermark[:watermark_length], used_blocks
    return watermark, used_blocks

def embed_watermark_quadrant(img_path, watermark):
    img = cv2.imread(img_path)
    y_img = get_y_img(img)

    wm_pointer = 0
    height_in_blocks = y_img.shape[0] // 4
    width_in_blocks = y_img.shape[1] // 4

    quadrant_size = 32

    quadrant_pointer = 0
    quadrants_height = (height_in_blocks - 1) // quadrant_size
    quadrants_width = (width_in_blocks - 1) // quadrant_size
    num_quadrants = quadrants_height * quadrants_width
    blocks_used_per_quadrant = np.ones(num_quadrants, dtype=list)

    for q in range(num_quadrants):
            blocks_used_per_quadrant[q] = list()

    used_blocks = list()
    wm_history = list()
    dep_blocks = list()

    while wm_pointer < len(watermark):
        quadrant_pointer = (quadrant_pointer + 1) % (num_quadrants)
        suggested_blocks = check_quadrant(blocks_used_per_quadrant[quadrant_pointer])
        pseudo_random_number = quadrant_pointer % len(suggested_blocks)
        chosen_block = suggested_blocks[pseudo_random_number]
        blocks_used_per_quadrant[quadrant_pointer].append(chosen_block)

        quadrant_i = quadrant_pointer // quadrants_width
        quadrant_j = quadrant_pointer % quadrants_width

        ii = 1 + chosen_block[0] + quadrant_size * quadrant_i
        jj = 1 + chosen_block[1] + quadrant_size * quadrant_j

        block_skipped = False
        if check_single_block(ii, jj, y_img):
            used_blocks.append((ii, jj))
            # Find pixel coordinates of the block
            block_i = ii * 4
            block_j = jj * 4
            # Get the block
            block = y_img[block_i:block_i + 4, block_j:block_j + 4]
            # transform the block
            block_dct = dct(block)
            # Embed the bits
            n_bits = min(embeddable_bits(block_dct) - 1, len(watermark) - wm_pointer)
            bits_in_block = np.concatenate(([1], watermark[wm_pointer: wm_pointer + n_bits]))
            e_block = embed(block_dct, bits_in_block, reversible = True)
            # Retransform the block
            block_embedded = idct(e_block)
            # test if the block is viable
            if (block_embedded < 0).any():
                # If doesn't work add a code beginning with zero
                bits_in_block = [0] * (n_bits + 1)
                e_block = embed(block_dct, bits_in_block)
                block_embedded = idct(e_block)
                block_skipped = True
                dep_blocks.append((ii, jj))
            # Put the block back
            y_img[block_i:block_i + 4, block_j:block_j + 4] = block_embedded
            #Check whether the mutation created a prediction
            if not check_single_block(ii, jj, y_img):
                #If so add a code with a leading 0
                bits_in_block = [0] * (n_bits + 1)
                e_block = embed(block_dct, bits_in_block)
                block_embedded = idct(e_block)
                #Put the new block in
                y_img[block_i:block_i + 4, block_j:block_j + 4] = block_embedded
                block_skipped = True

            if not block_skipped:
                # Otherwise update wm_pointer
                wm_pointer += n_bits
                wm_history.append((wm_pointer, (ii, jj)))

    return y_img

def extract_watermark_quadrant(y_img, watermark_length):
    used_blocks = list()
    watermark = list()
    height_in_blocks = y_img.shape[0] // 4
    width_in_blocks = y_img.shape[1] // 4
    wm_history = list()

    quadrant_size = 32

    quadrant_pointer = 0
    quadrants_height = (height_in_blocks - 1) // quadrant_size
    quadrants_width = (width_in_blocks - 1) // quadrant_size
    num_quadrants = quadrants_height * quadrants_width
    blocks_used_per_quadrant = np.ones(num_quadrants, dtype=list)

    for q in range(num_quadrants):
        blocks_used_per_quadrant[q] = list()

    while len(watermark) < watermark_length:
        quadrant_pointer = (quadrant_pointer + 1) % (num_quadrants)
        suggested_blocks = check_quadrant(blocks_used_per_quadrant[quadrant_pointer])
        pseudo_random_number = quadrant_pointer % len(suggested_blocks)
        chosen_block = suggested_blocks[pseudo_random_number]
        blocks_used_per_quadrant[quadrant_pointer].append(chosen_block)

        quadrant_i = quadrant_pointer // quadrants_width
        quadrant_j = quadrant_pointer % quadrants_width

        ii = 1 + chosen_block[0] + quadrant_size * quadrant_i
        jj = 1 + chosen_block[1] + quadrant_size * quadrant_j

        if check_single_block(ii, jj, y_img):
            used_blocks.append((ii,jj))
            # Find pixel coordinates of the block
            block_i = ii * 4
            block_j = jj * 4
            # Get the block
            block = y_img[block_i:block_i + 4, block_j:block_j + 4]
            # transform the block
            block_dct = dct(block)
            # extract the bits
            extracted = extraction(block_dct)
            # Check whether extracted is valid
            if extracted[0] == 1:
                watermark += extracted[1:]
                wm_history.append((len(watermark), (ii, jj)))

    return watermark

def embed_watermark_quadrant_one(img_path, watermark):
    img = cv2.imread(img_path)
    y_img = get_y_img(img)

    wm_pointer = 0
    height_in_blocks = y_img.shape[0] // 4
    width_in_blocks = y_img.shape[1] // 4

    quadrant_size = 32

    quadrant_pointer = 0
    quadrants_height = (height_in_blocks - 1) // quadrant_size
    quadrants_width = (width_in_blocks - 1) // quadrant_size
    num_quadrants = quadrants_height * quadrants_width
    blocks_used_per_quadrant = np.ones(num_quadrants, dtype=list)

    for q in range(num_quadrants):
            blocks_used_per_quadrant[q] = list()

    while wm_pointer < len(watermark):
        quadrant_pointer = (quadrant_pointer + 1) % (num_quadrants)
        a_block_has_been_embedded = False

        while not a_block_has_been_embedded:
            suggested_blocks = check_quadrant(blocks_used_per_quadrant[quadrant_pointer])
            pseudo_random_number = quadrant_pointer % len(suggested_blocks)
            chosen_block = suggested_blocks[pseudo_random_number]
            blocks_used_per_quadrant[quadrant_pointer].append(chosen_block)

            quadrant_i = quadrant_pointer // quadrants_width
            quadrant_j = quadrant_pointer % quadrants_width

            ii = 1 + chosen_block[0] + quadrant_size * quadrant_i
            jj = 1 + chosen_block[1] + quadrant_size * quadrant_j

            block_skipped = False
            if check_single_block(ii, jj, y_img):
                # Find pixel coordinates of the block
                block_i = ii * 4
                block_j = jj * 4
                # Get the block
                block = y_img[block_i:block_i + 4, block_j:block_j + 4]
                # transform the block
                block_dct = dct(block)
                # Embed the bits
                n_bits = min(embeddable_bits(block_dct) - 1, len(watermark) - wm_pointer)
                bits_in_block = np.concatenate(([1], watermark[wm_pointer: wm_pointer + n_bits]))
                e_block = embed(block_dct, bits_in_block, reversible = True)
                # Retransform the block
                block_embedded = idct(e_block)
                # test if the block is viable
                if (block_embedded < 0).any():
                    # If doesn't work add a code beginning with zero
                    bits_in_block = [0] * (n_bits + 1)
                    e_block = embed(block_dct, bits_in_block)
                    block_embedded = idct(e_block)
                    block_skipped = True
                # Put the block back
                y_img[block_i:block_i + 4, block_j:block_j + 4] = block_embedded
                #Check whether the mutation created a prediction
                if not check_single_block(ii, jj, y_img):
                    #If so add a code with a leading 0
                    bits_in_block = [0] * (n_bits + 1)
                    e_block = embed(block_dct, bits_in_block)
                    block_embedded = idct(e_block)
                    #Put the new block in
                    y_img[block_i:block_i + 4, block_j:block_j + 4] = block_embedded
                    block_skipped = True

                if not block_skipped:
                    # Otherwise update wm_pointer
                    wm_pointer += n_bits
                    a_block_has_been_embedded = True

    return y_img

def extract_watermark_quadrant_one(y_img, watermark_length):
    used_blocks = list()
    watermark = list()
    height_in_blocks = y_img.shape[0] // 4
    width_in_blocks = y_img.shape[1] // 4
    wm_history = list()

    quadrant_size = 32

    quadrant_pointer = 0
    quadrants_height = (height_in_blocks - 1) // quadrant_size
    quadrants_width = (width_in_blocks - 1) // quadrant_size
    num_quadrants = quadrants_height * quadrants_width
    blocks_used_per_quadrant = np.ones(num_quadrants, dtype=list)

    for q in range(num_quadrants):
        blocks_used_per_quadrant[q] = list()

    while len(watermark) < watermark_length:
        quadrant_pointer = (quadrant_pointer + 1) % (num_quadrants)
        block_found = False

        while not block_found:
            suggested_blocks = check_quadrant(blocks_used_per_quadrant[quadrant_pointer])
            if len(suggested_blocks) == 0:
                return watermark
            pseudo_random_number = quadrant_pointer % len(suggested_blocks)
            chosen_block = suggested_blocks[pseudo_random_number]
            blocks_used_per_quadrant[quadrant_pointer].append(chosen_block)

            quadrant_i = quadrant_pointer // quadrants_width
            quadrant_j = quadrant_pointer % quadrants_width

            ii = 1 + chosen_block[0] + quadrant_size * quadrant_i
            jj = 1 + chosen_block[1] + quadrant_size * quadrant_j

            if check_single_block(ii, jj, y_img):
                used_blocks.append((ii,jj))
                # Find pixel coordinates of the block
                block_i = ii * 4
                block_j = jj * 4
                # Get the block
                block = y_img[block_i:block_i + 4, block_j:block_j + 4]
                # transform the block
                block_dct = dct(block)
                # extract the bits
                extracted = extraction(block_dct)
                # Check whether extracted is valid
                if extracted[0] == 1:
                    watermark += extracted[1:]
                    wm_history.append((len(watermark), (ii, jj)))
                    block_found = True

    return watermark

def embed_watermark_quadrant_two(img_path, watermark):
    img = cv2.imread(img_path)
    y_img = get_y_img(img)
    block_used = list()

    wm_pointer = 0
    height_in_blocks = y_img.shape[0] // 4
    width_in_blocks = y_img.shape[1] // 4

    quadrant_size = 32

    quadrant_pointer = 0
    quadrants_height = (height_in_blocks - 1) // quadrant_size
    quadrants_width = (width_in_blocks - 1) // quadrant_size
    num_quadrants = quadrants_height * quadrants_width
    blocks_used_per_quadrant = np.ones(num_quadrants, dtype=list)

    for q in range(num_quadrants):
            blocks_used_per_quadrant[q] = list()

    while wm_pointer < len(watermark):
        block_skipped = False

        quadrant_pointer = (quadrant_pointer + 1) % (num_quadrants)
        suggested_blocks = check_quadrant(blocks_used_per_quadrant[quadrant_pointer])
        pseudo_random_number = quadrant_pointer % len(suggested_blocks)
        chosen_block = suggested_blocks[pseudo_random_number]
        blocks_used_per_quadrant[quadrant_pointer].append(chosen_block)

        quadrant_i = quadrant_pointer // quadrants_width
        quadrant_j = quadrant_pointer % quadrants_width

        ii = 1 + chosen_block[0] + quadrant_size * quadrant_i
        jj = 1 + chosen_block[1] + quadrant_size * quadrant_j

        # Find pixel coordinates of the block
        block_i = ii * 4
        block_j = jj * 4
        # Get the block
        block = y_img[block_i:block_i + 4, block_j:block_j + 4]
        # transform the block
        block_dct = dct(block)
        # Embed the bits
        n_bits = min(embeddable_bits(block_dct) - 1, len(watermark) - wm_pointer)
        bits_in_block = np.concatenate(([1], watermark[wm_pointer: wm_pointer + n_bits]))
        e_block = embed(block_dct, bits_in_block, reversible=True)
        # Retransform the block
        block_embedded = idct(e_block)
        # test if the block is viable
        if (block_embedded < 0).any():
            # If doesn't work add a code beginning with zero
            bits_in_block = [0] * (n_bits + 1)
            e_block = embed(block_dct, bits_in_block)
            block_embedded = idct(e_block)
            block_skipped = True
        # Put the block back
        y_img[block_i:block_i + 4, block_j:block_j + 4] = block_embedded
        #Check whether the mutation created a prediction
        if not check_single_block(ii, jj, y_img):
            #If so add a code with a leading 0
            bits_in_block = [0] * (n_bits + 1)
            e_block = embed(block_dct, bits_in_block)
            block_embedded = idct(e_block)
            #Put the new block in
            y_img[block_i:block_i + 4, block_j:block_j + 4] = block_embedded
            block_skipped = True

        if not block_skipped:
            # Otherwise update wm_pointer
            wm_pointer += n_bits
            block_used.append((ii, jj))

    return y_img

def extract_watermark_quadrant_two(y_img, watermark_length):
    watermark = list()
    blocks_used = list()
    height_in_blocks = y_img.shape[0] // 4
    width_in_blocks = y_img.shape[1] // 4

    quadrant_size = 32

    quadrant_pointer = 0
    quadrants_height = (height_in_blocks - 1) // quadrant_size
    quadrants_width = (width_in_blocks - 1) // quadrant_size
    num_quadrants = quadrants_height * quadrants_width
    blocks_used_per_quadrant = np.ones(num_quadrants, dtype=list)

    for q in range(num_quadrants):
        blocks_used_per_quadrant[q] = list()

    while len(watermark) < watermark_length:
        quadrant_pointer = (quadrant_pointer + 1) % (num_quadrants)
        suggested_blocks = check_quadrant(blocks_used_per_quadrant[quadrant_pointer])
        if len(suggested_blocks) == 0 or np.sum([len(blocks) for blocks in blocks_used_per_quadrant]) > 300:
            missing_bits = watermark_length - len(watermark)
            return False, np.concatenate((watermark, np.zeros(missing_bits)))

        pseudo_random_number = quadrant_pointer % len(suggested_blocks)
        chosen_block = suggested_blocks[pseudo_random_number]
        blocks_used_per_quadrant[quadrant_pointer].append(chosen_block)

        quadrant_i = quadrant_pointer // quadrants_width
        quadrant_j = quadrant_pointer % quadrants_width

        ii = 1 + chosen_block[0] + quadrant_size * quadrant_i
        jj = 1 + chosen_block[1] + quadrant_size * quadrant_j

        block_i = ii * 4
        block_j = jj * 4
        # Get the block
        block = y_img[block_i:block_i + 4, block_j:block_j + 4]
        # transform the block
        block_dct = dct(block)
        # extract the bits
        extracted = extraction(block_dct)
        # Check whether extracted is valid
        if len(extracted) > 1:
            if extracted[0] == 1:
                watermark += extracted[1:]
        blocks_used.append((ii, jj))

    return True, watermark[:watermark_length]