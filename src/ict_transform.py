import numpy as np

base_qp = 28

def get_w(matrix):
    Cf = np.array([
        [1,  1,  1,  1],
        [2,  1, -1, -2],
        [1, -1, -1,  1],
        [1, -2,  2, -1]
    ])
    return np.matmul(np.matmul(Cf, matrix), Cf.T)

def get_w_inverse(matrix):
    Ci = np.array([
        [1, 1, 1, 1],
        [1, 0.5, -0.5, -1],
        [1, -1, -1, 1],
        [0.5, -1, 1, -0.5]
    ])
    return np.matmul(np.matmul(Ci.T, matrix), Ci)

def qstep(qp):
    if qp < 6:
        return 0.625 * np.power(1.2246, qp)
    else:
        return qstep(qp % 6) * np.power(2, qp // 6)

def mf(i, j, qp):
    if qp >= 5:
        if (i, j) in [(0, 0), (2, 0), (2, 2), (0, 2)]:
            return 7282
        elif (i, j) in [(1, 1), (1, 3), (3, 1), (3, 3)]:
            return 2893
        else:
            return 4559
    else:
        if (i, j) in [(0, 0), (2, 0), (2, 2), (0, 2)]:
            return [13107, 11916, 10082, 9362, 8192][qp]
        elif (i, j) in [(1, 1), (1, 3), (3, 1), (3, 3)]:
            return [5243, 4660, 4194, 3647, 3355][qp]
        else:
            return [8066, 7490, 6554, 5825, 5243][qp]

def v(i, j, qp):
    if qp >= 5:
        if (i, j) in [(0, 0), (2, 0), (2, 2), (0, 2)]:
            return 18
        elif (i, j) in [(1, 1), (1, 3), (3, 1), (3, 3)]:
            return 29
        else:
            return 23
    else:
        if (i, j) in [(0, 0), (2, 0), (2, 2), (0, 2)]:
            return [10, 11, 13, 14, 16][qp]
        elif (i, j) in [(1, 1), (1, 3), (3, 1), (3, 3)]:
            return [16, 18, 20, 23, 25][qp]
        else:
            return [13, 14, 16, 18, 20][qp]

def qbits(qp):
    return 15 + np.floor(qp / 6)


def dct(matrix, qp=base_qp):
    w = get_w(matrix)

    y = np.zeros((4,4))
    for i in range(4):
        for j in range(4):
            y[i, j] = round(w[i, j] * mf(i, j, qp) / np.power(2, qbits(qp)))

    return y

def idct(matrix, qp=base_qp):
    w = np.zeros((4,4))
    for i in range(4):
        for j in range(4):
            w[i, j] = matrix[i, j] * v(i, j, qp) * np.power(2, np.floor(qp/6))

    return np.round(get_w_inverse(w / 64))

def transform(matrix, it, qp=base_qp):
    i = matrix
    print(i)
    for ii in range(it):
        d = dct(matrix, qp=qp)
        i = idct(d, qp=qp)
        print(d)
        print(i)
    print(np.sum(np.abs(matrix - i)))

def get_dif(matrix, qp):
    w = dct(matrix, qp=qp)
    x = idct(w, qp=qp)
    return np.sum(np.abs(matrix - x))