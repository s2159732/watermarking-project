from ict_transform import *
import cv2
from prediction_modes import *

def analysis(img_path):
    img = cv2.imread(img_path)
    y_img = get_y_img(img)

    tot = 0
    pos_emb = 0

    blocks_height = y_img.shape[0] // 4
    blocks_width = y_img.shape[1] // 4
    for ii in range(blocks_height):
        for jj in range(blocks_width):
            i = ii * 4
            j = jj * 4
            dct_block = dct(y_img[i:i+4, j:j+4])
            if np.sum((dct_block.flatten() == 1)) >= 4:
                pos_emb += 1
            tot += 1

    return pos_emb, tot