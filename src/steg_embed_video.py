import cv2

from iframes import *
from block_selection import *
from prediction_modes import *
from ict_transform import *
from distribute_watermark import *
from embedding import *
from remake_image import *
from watermark_to_image import *
from error_correcting_code import *
import os
import numpy as np



def steg_embed_video(video_path, watermark_path, folder = "frames"):
    iframes = get_iframes(video_path, folder_path=folder)
    watermark = image_to_watermark(watermark_path)
    distributed_watermark = distribute_watermark(watermark)

    for ii in range(len(iframes)):
        frame_watermark = distributed_watermark[ii % 16]
        frame_watermark = np.concatenate((frame_index_to_bin_array(ii % 16), frame_watermark))
        encoded_frame_watermark = dummy_encode(frame_watermark)
        frame_path = f"{folder}/frame_{iframes[ii]}.jpg"
        yimg = embed_watermark_quadrant_two(frame_path, encoded_frame_watermark)
        img = insert_y(yimg, frame_path)
        cv2.imwrite(frame_path, img)
        print(f"{ii+1}/{len(iframes)}")

    # write_to_a_video("test", "videos/desk_ifi4.mp4", "frames", iframes)

def write_to_a_video(new_video_path, orig_video_path, frame_folder, iframe_index):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video = cv2.VideoWriter(new_video_path, fourcc, 30, (1280, 720))
    orig_video = cv2.VideoCapture(orig_video_path)

    frame_index = 0
    while True:
        print(frame_index)
        ret, frame = orig_video.read()
        if not ret:
            break
        if frame_index in iframe_index:
            add_frame = cv2.imread(f"{frame_folder}/frame_{frame_index}.jpg")
        else:
            add_frame = frame
        add_frame = frame
        frame_index += 1
        video.write(add_frame)
        cv2.imshow("output", add_frame)
        if cv2.waitKey(1) & 0xFF == ord('s'):
            break

    video.release()
    orig_video.release()
    cv2.destroyAllWindows()

# steg_embed_video(sys.argv[0], sys.argv[1])
steg_embed_video("videos/desk2.mp4", "img/watermark.jpg")